package final

import (
	"fmt"
	"testing"
)

func TestStart(t *testing.T) {
	err := NewPid("finaltest")
	if err != nil {
		t.Errorf("New Error %s", err)
	}
	if err := Add(end); err != nil {
		t.Errorf("Add Error %s", err)
	}
}

func end() {
	fmt.Println("Executed final function 'end()'")
}
