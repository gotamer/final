// This is a final() method for the Go language to compliment the init() method.
//
// It watches the underlying OS'es signal and executes in case of an exit request.
//
// Multiple final functions can be added.
// If you are compiling your program from small pieces or from multiple libs
// you may have a final() function in each of them.
// They will be executed in the order added.
//
// A pid file in the OS'es temp directory will be created.
// This can be used to shut down the application even if it is running as a deamon.
//
// Create a pid file and start watching the os signals
//		err := final.NewPid(applicationName string)
//
// Here we are adding a function called theEnd() to the list of functions to be executed before shutdown
//		err := final.Add(theEnd)
//
// Then
//	func theEnd() {
//		yourfile.close()
//	}
package final

import (
	"errors"
	"fmt"
	"os"
	"os/signal"

	"bitbucket.org/gotamer/cfg"
)

const (
	PS = string(os.PathSeparator)
)

type pid struct {
	Id   int      // pid id
	Name string   // Application Name
	List []func() // Final functions to execute before exiting
	err  error    // Last error
}

var (
	thisPid *pid
)

// Create a pid file and start watching the os signals
func NewPid(name string) (err error) {
	var p *pid
	if p, err = newPid(name); err != nil {
		return
	}

	if err = p.write(); err != nil {
		return
	}
	thisPid = p
	go signalCatcher()
	return
}

// Adds a function to be executed before shutdown
func Add(f func()) (err error) {
	if thisPid == nil {
		err = errors.New("No PID see: NewPid(name string)")
		return
	}
	thisPid.List = append(thisPid.List, f)
	return
}

func signalCatcher() {
	defer StopThis()
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, os.Kill)

	// Block until a signal is received.
	s := <-c
	fmt.Println("### Stopping ###")
	fmt.Println("Got signal:", s)
	StopThis()

	/*
		go func() {
			for sig := range c {
				fmt.Println(sig)
				StopThis()
			}
		}()
	*/
}

// Stop current process
func StopThis() {
	count := len(thisPid.List)
	for i := 0; i < count; i++ {
		thisPid.List[i]()
	}
	fmt.Println(thisPid)
	os.Remove(thisPid.filePath())
	os.Exit(0)
}

// Stop an application by pid file path
// Do not try to stop self with this, use theEnd() to stop self
func StopThat(name string) (ok bool, err error) {
	var p *pid
	if p, err = newPid(name); err != nil {
		return
	}

	var process *os.Process
	if p.IsRunning() {
		err = p.read()
		if err != nil {
			return
		}
		process, err = os.FindProcess(p.Id)
		if err != nil {
			return
		}
		err = process.Signal(os.Interrupt)
		if err != nil {
			return
		}
		var state *os.ProcessState
		state, err = process.Wait()
		if err != nil {
			return
		}
		if ok = state.Success(); ok {
			err = p.removePid()
		}
	}
	return
}

// Check if an application is running
func (p *pid) IsRunning() bool {
	s, err := os.Stat(p.filePath())
	if err != nil {
		fmt.Println("### NOT RUNNING ###")
		fmt.Println("os Stat: ", s)
		fmt.Println("os Stat err: ", err)
		return false
	}
	return true
}

func newPid(name string) (p *pid, err error) {
	if name == "" {
		err = errors.New("Empty Application name!")
		return
	}
	p = new(pid)
	p.Id = os.Getpid()
	p.Name = name
	return
}

func (p *pid) filePath() string {
	return os.TempDir() + PS + "." + p.Name + ".pid"
}

// write the pif id to a file
func (p *pid) write() error {
	return cfg.Save(p.filePath(), p)
}

// read the pid id from the pid file
func (p *pid) read() (err error) {
	var o = new(pid)
	if err = cfg.Load(p.filePath(), o); err == nil {
		p.Id = o.Id
	} else {
		fmt.Println("Pid read error: ", err)
	}
	return
}

// clean up -> removes the pid file
func (p *pid) removePid() error {
	return os.Remove(p.filePath())
}

/*
func (p *pid) byteId() []byte {
	return conv.Itob(int64(p.Id))
}

func (p *pid) byteToInt(b []byte) int {
	return int(conv.Btoi(b))
}
*/
