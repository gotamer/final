gotamer/final
=============
This is a final() method for the Go language to compliment the init() method.

It watches the underlying OS'es signal and executes in case of an exit request.

Multiple final functions can be added.
If you are compiling your program from small pieces or from multiple libs
you may have a final() function in each of them.
They will be executed in the order added.

A pid file in the OS'es temp directory will be created.
This can be used to shut down the application even if it is running as a deamon.

Create a pid file and start watching the os signals
		err := final.NewPid(applicationName string)

Here we are adding a function called theEnd() to the list of functions to be executed before shutdown
		err := final.Add(theEnd)

Then
	func theEnd() {
		yourfile.close()
	}


For example of use see test file and:

	gotamer/mail
	gotamer/earthporn

Note
------
This version is not backwards compatable. The apllication name is no longer optional, it is requiered!
